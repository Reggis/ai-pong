#ifndef PADDLE_H
#define PADDLE_H

#include <QPainter>
#include <QGraphicsScene>
#include <QGraphicsItem>

class Paddle : public QGraphicsItem
{
public:
    Paddle(bool AI, bool f);
    QRectF boundingRect() const;
    void paint(QPainter *painter,
               const QStyleOptionGraphicsItem *option,
               QWidget *widget);
    int direction;

    protected:
        void advance(int phase);

    private:
        bool AI;
        bool first;

};

#endif // PADDLE_H
