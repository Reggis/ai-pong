#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>
#include <QKeyEvent>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    scene = new QGraphicsScene(this);

    ui->game->setScene(scene);

    ui->game->setRenderHint(QPainter::Antialiasing);

    scene->setSceneRect(-300, -200, 600, 400);

    QPen myPen = QPen(Qt::red);


    QGraphicsRectItem* bottom = new QGraphicsRectItem(QRect(0,0,600,1));
    bottom->setPos(-300,200);
    QGraphicsRectItem* top = new QGraphicsRectItem(QRect(0,0,600,1));
    top->setPos(-300,-200);
    QGraphicsRectItem* left = new QGraphicsRectItem(QRect(0,0,1,400));
    left->setPos(-300,-200);
    QGraphicsRectItem* right = new QGraphicsRectItem(QRect(0,0,1,400));
    right->setPos(300,-200);

    scene->addItem(bottom);
    scene->addItem(top);
    scene->addItem(left);
    scene->addItem(right);

    ball = new Ball();
    scene->addItem(ball);

    player1 = new Paddle(false, true);
    player1->setPos(250,-50);
    scene->addItem(player1);

    player2 = new Paddle(true, false);
    player2->setPos(-250,-50);
    scene->addItem(player2);


    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()),scene, SLOT(advance()));

    p1Score=0;
    p2Score=0;

    isGame = false;


    qDebug() << "Neural learning " <<endl;

    connect(ui->startButton, SIGNAL(clicked()), this, SLOT(StartGame()));
    connect(ui->stopButton, SIGNAL(clicked()), this, SLOT(StopGame()));




    QFile file("/home/reggis/QtProjects/AIPong2/cases.txt");
    if(!file.open(QIODevice::ReadOnly)) {
        return;
    }

    QTextStream in(&file);

    while(!in.atEnd()) {
        QString line = in.readLine();
        QStringList fields = line.split(",");
        QList<int> vlist;
        for(int k=0; k<fields.size(); k++)
        {
            vlist.append(fields[k].toInt());
        }
        this->learnCases.append(vlist);
    }

    file.close();


          int numLayers = 6, lSz[6] = {3,10,10,4,4,1};
          double beta = 0.1, alpha = 0.1;
          long num_iter = 2000000;

          double lastError=100000;
          double prelastError=2000000;

          nn = new NeuralNetwork(numLayers, lSz, beta, alpha);

                qDebug()<< endl <<  "Now training the network...." << endl;
                 for (long i=0; i<num_iter ; i++)
                 {

                        int num = rand()%(learnCases.size()-2000);
//                        if(learnCases[num][3] > -200 && learnCases[num][3] < 200)
                        {
                            double inArray[3] = {(double)(300+learnCases[num][0])/600, (double)(200+learnCases[num][1])/400, (double)learnCases[num][2]/360};
                            double answerArray[1] = {(double)(learnCases[num][3]+200)/400};

                            nn->backPropagate(inArray, answerArray);
                        }

                     if ( i%(num_iter/10) == 0 )
                     {

                         double error =0;
                         for(int j=0; j<1000; j++)
                         {
                              int num = rand()%2000 + learnCases.size()-2000;
//                              if(learnCases[num][3] > -200 && learnCases[num][3] < 200)
                              {
                                  double inArray[3] = {(double)(300+learnCases[num][0])/600, (double)(200+learnCases[num][1])/400, (double)learnCases[num][2]/360};
                                  double answerArray[1] = {(double)(learnCases[num][3]+200)/400};

                                  nn->forwardActivation(inArray);

                                  double er = nn->error(answerArray);

                                  error += (1+er)*(1+er)-1;
                              }

                        }
                         error /= 2;

                         qDebug()<<  endl <<  "MSE:  " << error
                             << "... Training..." << endl;
                          if(error > prelastError)
                          {
                              qDebug() <<"end of training";
                              break;
                          }
                          else
                          {
                              prelastError = lastError;
                              lastError = error;
                          }
                     }


                 }

                 for (int i = 0 ; i < 8 ; i++ )
                 {
                     int num = rand()%1000 + learnCases.size()-1000;
                     double inArray[3] = {(double)(300+learnCases[num][0])/600, (double)(200+learnCases[num][1])/400, (double)learnCases[num][2]/360};
                     double answerArray[1] = {(double)(learnCases[num][3]+200)/400};
                     nn->forwardActivation(inArray);
                     qDebug() << inArray[0]<< "  " << inArray[1]<< "  "  << inArray[2]<< "  " << nn->out(0) << "Proper answer: " << answerArray[0] << endl;
                 }

    qDebug() << "End of test";

}

double MainWindow::predictMove()
{
    double inArray[3] = {(double)(ball->pos().x()+300)/600, (double)(ball->pos().y()+200)/400, (double)ball->rotation()/360};
    nn->forwardActivation(inArray);

    double pred = nn->out(0);
    return pred;
}

bool MainWindow::isAI()
{
    return this->ui->AIBox->isChecked();
}

void MainWindow::StartGame()
{
    qDebug() << "Start";
    timer->start(50);
}

void MainWindow::StopGame()
{
    timer->stop();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::p1GoodMove(QVector<QVector<int> > moves)
{
    if(this->ui->learnBox->isChecked())
    {
        QFile file("/home/reggis/QtProjects/AIPong2/cases.txt");
        if (file.open(QIODevice::Append )) {
            QTextStream stream(&file);
            for(QVector<int> move : moves)
            {
                if(move.size() >= 3)
                    stream << -move[0] << ", " << move[1] << ", "<< move[2] << ", " << move[3] << endl;
            }
        }
    }
}
void MainWindow::p2GoodMove(QVector<QVector<int> > moves)
{
    if(this->ui->learnBox->isChecked())
    {
        QFile file("/home/reggis/QtProjects/AIPong2/cases.txt");
        if (file.open(QIODevice::Append )) {
            QTextStream stream(&file);
            for(QVector<int> move : moves)
            {
                if(move.size() >= 3)
                    stream << move[0] << ", " << move[1] << ", "<< move[2] << ", " << move[3] << endl;
            }
        }
    }
}
void MainWindow::p1Goal()
{
    p1Score++;
    ui->p1score->setText(QString::number(p1Score));
}
void MainWindow::p2Goal()
{
    p2Score++;
    ui->p2score->setText(QString::number(p2Score));
}

void MainWindow::keyPressEvent( QKeyEvent * event )
{
    if( event->key() == Qt::Key_W )
    {
        player2->direction = -1;
    }
    if( event->key() == Qt::Key_S )
    {
        player2->direction = 1;
    }
    if( event->key() == Qt::Key_O)
    {
        player1->direction = -1;

    }
    if( event->key() == Qt::Key_L)
    {
        player1->direction = 1;
    }
}

void MainWindow::keyReleaseEvent(QKeyEvent * event)
{
    if( event->key() == Qt::Key_W )
    {
        player2->direction = 0;
    }
    if( event->key() == Qt::Key_S )
    {
        player2->direction = 0;
    }
    if( event->key() == Qt::Key_O)
    {
        player1->direction = 0;
    }
    if( event->key() == Qt::Key_L)
    {
        player1->direction = 0;
    }
}
