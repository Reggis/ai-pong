#ifndef NEURALNETWORK_H
#define NEURALNETWORK_H


class NeuralNetwork
{
public:
    NeuralNetwork(int num, int *sizes, double rate, double alpha);
    ~NeuralNetwork();
    void backPropagate(double *input, double *target);

    void forwardActivation(double *in);

    double error(double *target) const;

    double out(int i) const;

private:
    double **output;
    double **deltaError;
    double ***weights;

    int numOfLayers;
    int *sizesOfLayers;

    double learningRate;
    double momentum;

    double ***previousDwt;

    double sigmoid(double input);


};

#endif // NEURALNETWORK_H
