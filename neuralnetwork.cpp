#include "neuralnetwork.h"
#include "time.h"
#include "stdlib.h"
#include <math.h>
#include <QDebug>

NeuralNetwork::NeuralNetwork(int num, int *sizes, double rate, double alpha)
{
    learningRate = rate;
    momentum = alpha;

    numOfLayers = num;
    sizesOfLayers = new int[numOfLayers];

    for(int i=0; i<numOfLayers; i++)
    {
        sizesOfLayers[i]=sizes[i];
    }

    output = new double*[numOfLayers];

    for(int i=0; i<numOfLayers; i++)
    {
        output[i] = new double[sizesOfLayers[i]];
    }

    deltaError = new double*[numOfLayers];

    for(int i=1; i<numOfLayers; i++)
    {
        deltaError[i] = new double[sizesOfLayers[i]];
    }

    weights = new double**[numOfLayers];

    for(int i=1;i<numOfLayers;i++){
        weights[i]=new double*[sizesOfLayers[i]];
    }
    for(int i=1;i<numOfLayers;i++){
        for(int j=0;j<sizesOfLayers[i];j++){
            weights[i][j]=new double[sizesOfLayers[i-1]+1];
        }
    }


    previousDwt = new double**[numOfLayers];

    for(int i=1;i<numOfLayers;i++){
        previousDwt[i]=new double*[sizesOfLayers[i]];
    }
    for(int i=1;i<numOfLayers;i++){
        for(int j=0;j<sizesOfLayers[i];j++){
            previousDwt[i][j]=new double[sizesOfLayers[i-1]+1];
        }
    }
    srand((unsigned)(time(NULL)));
    for(int i=1; i<numOfLayers; i++)
    {
        for(int j=0; j< sizesOfLayers[i]; j++)
        {
            for(int k=0; k<sizesOfLayers[i-1]; k++)
            {
                weights[i][j][k]=(double)(rand())/(RAND_MAX/2) - 1;
                previousDwt[i][j][k] = (double)0.0;
            }
        }
    }

//    for(int i=1; i<numOfLayers; i++)
//    {
//        for(int j=0; j< sizesOfLayers[i]; j++)
//        {
//            for(int k=0; k<sizesOfLayers[i-1]; k++)
//            {
//                qDebug() << weights[i][j][k];
//            }
//        }
//    }



     // Note that the following variables are unused,
    //
    // delta[0]
    // weight[0]
    // prevDwt[0]
    //  I did this intentionaly to maintains consistancy in numbering the layers.
    //  Since for a net having n layers, input layer is refered to as 0th layer,
    //  first hidden layer as 1st layer and the nth layer as output layer. And
    //  first (0th) layer just stores the inputs hence there is no delta or weigth
    //  values corresponding to it.


}

NeuralNetwork::~NeuralNetwork()
{
    int i;
    for(int i=0;i<numOfLayers;i++)
      delete[] output[i];
    delete[] output;

    //  free delta
    for(i=1;i<numOfLayers;i++)
      delete[] deltaError[i];
    delete[] deltaError;

    //  free weight
    for(i=1;i<numOfLayers;i++)
      for(int j=0;j<sizesOfLayers[i];j++)
         delete[] weights[i][j];
    for(i=1;i<numOfLayers;i++)
     delete[] weights[i];
    delete[] weights;

    //  free prevDwt
    for(i=1;i<numOfLayers;i++)
     for(int j=0;j<sizesOfLayers[i];j++)
         delete[] previousDwt[i][j];
    for(i=1;i<numOfLayers;i++)
     delete[] previousDwt[i];
    delete[] previousDwt;

    //  free layer info
    delete[] sizesOfLayers;
}

double NeuralNetwork::sigmoid(double input)
{
    return (double)(1/(1+exp(-input)));
}

double NeuralNetwork::error(double *target) const
{
    double mse =0;
    for(int i=0; i<sizesOfLayers[numOfLayers-1]; i++)
    {
        mse+=(target[i]-output[numOfLayers-1][i])*(target[i]-output[numOfLayers-1][i]);
    }
    return mse/2;
}

double NeuralNetwork::out(int i) const
{
    return output[numOfLayers-1][i];
}

void NeuralNetwork::forwardActivation(double *in)
{
    double sum;

    for(int i=0; i<sizesOfLayers[0]; i++)
    {
        output[0][i]=in[i];
    }

    for(int i=1; i<numOfLayers; i++)
    {
        for(int j=0; j<sizesOfLayers[i]; j++)
        {
            sum =0.0;
            for(int k=0; k<sizesOfLayers[i-1]; k++)
            {
                sum += output[i-1][k]*weights[i][j][k];

            }
            sum += weights[i][j][sizesOfLayers[i-1]];
            output[i][j]=sigmoid(sum);
        }
    }
}

void NeuralNetwork::backPropagate(double *input, double *target)
{
    double sum;
    forwardActivation(input);

    //delta for output layers
    for(int i=0; i<sizesOfLayers[numOfLayers-1]; i++)
    {
        deltaError[numOfLayers-1][i] = output[numOfLayers-1][i]*(1-output[numOfLayers-1][i])*(target[i]-output[numOfLayers-1][i]);
    }

    //deltas for hidden layers
    for(int i=numOfLayers-2; i>0; i--)
    {
        for(int j=0; j<sizesOfLayers[i];j++)
        {
            sum=0;
            for(int k=0; k<sizesOfLayers[i+1];k++)
            {
                sum+=deltaError[i+1][k]*weights[i+1][k][j];
            }
            deltaError[i][j] = output[i][j]*(1-output[i][j])*sum;
        }
    }

    //momentum
    for(int i=1; i<numOfLayers; i++)
    {
        for(int j=0; j<sizesOfLayers[i]; j++)
        {
            for(int k=0; k<sizesOfLayers[i-1]; k++)
            {
                weights[i][j][k]+=momentum*previousDwt[i][j][k];
            }
            weights[i][j][sizesOfLayers[i-1]]+=momentum*previousDwt[i][j][sizesOfLayers[i-1]];
        }
    }

    for(int i=1; i<numOfLayers; i++)
    {
        for(int j=0; j<sizesOfLayers[i]; j++)
        {
            for(int k=0; k<sizesOfLayers[i-1]; k++)
            {
                previousDwt[i][j][k] = learningRate*deltaError[i][j]*output[i-1][k];
                weights[i][j][k] +=previousDwt[i][j][k];
            }
            previousDwt[i][j][sizesOfLayers[i-1]] = learningRate*deltaError[i][j];
            weights[i][j][sizesOfLayers[i-1]] += previousDwt[i][j][sizesOfLayers[i-1]];
        }
    }
}
