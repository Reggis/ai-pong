#include "paddle.h"
#include <QMainWindow>

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    double predictMove();
    bool isAI();
};

Paddle::Paddle(bool ai, bool f):AI(ai),first(f)
{
    direction=0;
}

QRectF Paddle::boundingRect() const
{
    return QRect(0,0,5,100);
}

void Paddle::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    QRectF rect = boundingRect();

    // basic collision detection

    if(scene()->collidingItems(this).isEmpty())
    {
        // no collision
        QPen pen(Qt::black, 5);
        painter->setPen(pen);
    }
    else
    {

    }
    painter->drawRect(rect);

}
void Paddle::advance(int phase)
{
    if(!phase) return;

    MainWindow *mw = (MainWindow*)scene()->parent();

    if(AI && mw->isAI())
    {    
        double position = mw->predictMove();
        int y = position*400 -200;
        int move =0;
        if(abs(pos().y() - y) <= 10)
            move = y - pos().y();
        else
        {
            if(pos().y() - y > 0)
                move = -10;
            else
                move=10;
        }
        setPos(pos().x(), pos().y()+move);
    }
    else
        setPos(pos().x(), pos().y()+10*direction);

}
