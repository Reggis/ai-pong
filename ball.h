#ifndef BALL_H
#define BALL_H

#include <QPainter>
#include <QGraphicsScene>
#include <QGraphicsItem>
#include <QVector>

class Ball : public QGraphicsItem
{
public:
    Ball();
    QRectF boundingRect() const;
    void paint(QPainter *painter,
               const QStyleOptionGraphicsItem *option,
               QWidget *widget);

    protected:
        void advance(int phase);

    private:
        qreal angle, speed;
        void doCollision(QGraphicsItem* obj);
        QVector<QVector<int> > moves;
        void restart();
};

#endif // BALL_H
