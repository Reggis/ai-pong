#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QGraphicsScene>
#include <QTimer>

#include <sstream>
#include <fstream>
#include <iostream>
#include <QString>
#include <QFile>
#include <QVector>

#include "ball.h"
#include "paddle.h"
#include "neuralnetwork.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void p1Goal();
    void p2Goal();
    void p1GoodMove(QVector<QVector<int> > moves);
    void p2GoodMove(QVector<QVector<int> > moves);
    double predictMove();
    bool isAI();

private slots:
    void StartGame();
    void StopGame();

private:
    Ui::MainWindow *ui;
    QGraphicsScene *scene;
    QTimer *timer;

    Ball *ball;
    NeuralNetwork *nn;

    Paddle *player1;
    Paddle *player2;

    int p1Score;
    int p2Score;

    void keyPressEvent( QKeyEvent * event );
    void keyReleaseEvent( QKeyEvent * event );

    QVector< QList<int> > learnCases;

    bool isGame;
};

#endif // MAINWINDOW_H
