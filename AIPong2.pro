#-------------------------------------------------
#
# Project created by QtCreator 2015-12-13T18:15:49
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = AIPong2
TEMPLATE = app
CONFIG += c++11


SOURCES += main.cpp\
        mainwindow.cpp \
    paddle.cpp \
    ball.cpp \
    neuralnetwork.cpp

HEADERS  += mainwindow.h \
    paddle.h \
    ball.h \
    neuralnetwork.h

FORMS    += mainwindow.ui
