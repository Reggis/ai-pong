#include "ball.h"
#include <QDebug>
#include <QMainWindow>
#include "paddle.h"

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    void p1Goal();
    void p2Goal();
    void p1GoodMove(QVector<QVector<int> > moves);
    void p2GoodMove(QVector<QVector<int> > moves);
};

Ball::Ball()
{
        // random start rotation
        angle = (80);
        setRotation(angle);

        // set the speed
        speed = 10;  // 5 pixels

        // random start position
        int startX = 0;
        int startY = 0;


        setPos(mapToParent(startX, startY));


}

QRectF Ball::boundingRect() const
{
    return QRect(0,0,10,10);
}

void Ball::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    QRectF rect = boundingRect();

    // basic collision detection

    if(scene()->collidingItems(this).isEmpty())
    {
        // no collision
        QPen pen(Qt::green, 5);
        painter->setPen(pen);
    }
    else
    {
        // collision !!!
        QPen pen(Qt::red, 5);
        painter->setPen(pen);

        // set the position
//        QGraphicsItem *obj = scene()->collidingItems(this).last();
//        doCollision(obj);
    }
    painter->drawEllipse(rect);

}
void Ball::advance(int phase)
{
    if(!phase) return;

    QPointF location = this->pos();

    setPos(mapToParent(0, -speed));
    if(pos().x() >= 300)
        setPos(300, location.y());
    if(pos().x() <= -300)
        setPos(-300, location.y());
    if(pos().y() >= 200)
        setPos(location.x(), 200);
    if(pos().y() <= -200)
        setPos(location.x(), -200);

    if(!scene()->collidingItems(this).isEmpty())
    {
        QGraphicsItem *obj = scene()->collidingItems(this).last();
        doCollision(obj);
    }

    QVector<int> frameData;
    frameData.push_back(pos().x());
    frameData.push_back(pos().y());
    frameData.push_back(rotation());
    for(QGraphicsItem *o : scene()->items())
    {
        if(dynamic_cast<Paddle*>(o) && ((o->pos().x() >0 && rotation() >=0 && rotation() <=180 && this->pos().x() > 0) || (o->pos().x() <0  && rotation() >180 && rotation() <=360 && this->pos().x() < 0) ) )
        {
            frameData.push_back(o->pos().y());
            this->moves.push_back(frameData);
        }
    }


}

void Ball::doCollision(QGraphicsItem *obj)
{
    MainWindow *mw = (MainWindow*)scene()->parent();

    Paddle *pad = dynamic_cast<Paddle*>(obj);

    if(pad)
    {
        setRotation(360-rotation());
        if(obj->pos().x() < 0)
        {
            mw->p1GoodMove(moves);
            moves.clear();
            setRotation(rotation() - (obj->pos().y()+50 - pos().y())/5);

        }
        else
        {
            mw->p2GoodMove(moves);
            moves.clear();
            setRotation(rotation() + (obj->pos().y()+50 - pos().y())/5);
        }

        setRotation((int)rotation() % 360);


        setPos(obj->pos().x(), pos().y());
    }
    else
    {

        if(obj->pos().y() == 200 || (obj->pos().y() == -200 &&pos().y() <= -190))
        {
            setRotation(180-rotation());
        }
        else
        {
           setRotation(360-rotation());
            if(obj->pos().x() < 0)
            {

                mw->p2Goal();
                moves.clear();
            }
            else
            {
                mw->p1Goal();
                moves.clear();
            }
            restart();
        }
    }
    setPos(mapToParent(0, -speed));
    speed += 0.1;
//    setPos(mapToParent(0, -speed));




    // check if the new position is in bounds
    QPointF newPoint = mapToParent(-(boundingRect().width()), -(boundingRect().width() + 2));

    if(!scene()->sceneRect().contains((newPoint)))
    {
        // move back in bounds
        newPoint = mapToParent(0,0);
        setPos(newPoint);
    }
    else
    {
        // set the new position
        setPos(newPoint);
    }
}

void Ball::restart()
{
    setPos(0, 0);
    setRotation(80);
}

